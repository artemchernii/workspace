const inputValue = document.querySelector("#search");
const searchButton = document.querySelector(".searchButton");
const nameContainer = document.querySelector(".main__profile-name");
const unContainer = document.querySelector(".main__profile-username");
const reposContainer = document.querySelector(".main__profile-repos");
const urlContainer = document.querySelector(".main__profile-url");
const imgContainer = document.querySelector(".main__profile-img");
const locContainer = document.querySelector(".main__profile-location");
const bioContainer = document.querySelector(".main__profile-bio");


const client_id = "Iv1.658a8f79a1c85730";
const client_secret = "56a33efb15c3a7d402305c056453fae51547ffba";

const fetchUsers = async (user) => {
	const api_call = await fetch(`https://api.github.com/users/${user}?client_id=${client_id}&client_secret=${client_secret}`);

	const data = await api_call.json();
	return { data }
};

const showData = () => {
	fetchUsers(inputValue.value).then((res) => {
		console.log(res.data);
		if(res.data.message == "Not Found") {
			alert("User not found, try another name")
		} else {
			unContainer.innerHTML = `Username: <span class="main__profile-value">${res.data.login}</span>`;
			urlContainer.innerHTML = `Url: <span class="main__profile-value">${res.data.html_url}</span>`;
			imgContainer.innerHTML = `This is John: <img class="main__profile-image" src=${res.data.avatar_url} alt="Photo">`;
			if(res.data.bio != undefined && res.data.bio != null) {
				bioContainer.innerHTML = `Bio: <span class="main__profile-value">${res.data.bio}</span>`;
			}
			if(res.data.location !== undefined && res.data.location !== null) {
				locContainer.innerHTML = `Location: <span class="main__profile-value">${res.data.location}</span>`
			}
			if(res.data.name != undefined && res.data.name != null) {
				nameContainer.innerHTML = `Name: <span class="main__profile-value">${res.data.name}</span>`;
			}
			if(res.data.public_repos != undefined && res.data.public_repos != null) {
				reposContainer.innerHTML = `Reposetories: <span class="main__profile-value">${res.data.public_repos}</span>`;
			}		
		}
	});
}

searchButton.addEventListener("click", () => {
	showData();
});

