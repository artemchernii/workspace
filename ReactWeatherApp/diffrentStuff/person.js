export class Person {
	constructor(name,nickname) {
		this.name = name;
		this.nickname = nickname;
	}
	walk() {
		console.log("Walk");
	}
}








import Employee from './teacher'; 



const person = {
	name: "Artem",
	talk() {
		console.log(this);
	}

};
const walk = person.talk.bind(person);
person.talk();
console.log(walk);
walk();

const sqr = (number) =>  number*number;

(sqr(2) !== 4) ? console.log(sqr(2)) : console.log("sorry");

const jobs = [
	{ id: 1, isActive : true},
	{ id: 2, isActive : false},
	{ id: 2, isActive : true}
];

// const activeJobs =  jobs.filter(jobs => jobs.isActive);

// console.log(activeJobs);

// const person = {
// 	talk() {
// 		setTimeout(() => { console.log("this", this) }, 1000);
// 	}
// };

// person.talk();

// const colors = ['red' , 'green' , 'blue'];

// const items = colors.map((color) => `<li>${color}</li>`);
// console.log(items);

// const address = {
// 	street: 'Kosmonavtiv',
// 	city: '',
// 	country: ''
// };

// const { street, city, country } = address;
// const { street : st } = address;

// console.log(st);

const first = [1,2,3];
const second = [4,5,6];

const third = [...first, ...second];
console.log(third);


const artem = { name : "Artem" , age : 24 };

console.log(artem);

const chernii = { ...artem, secondName: 'Chernii'};

const another = { ...chernii };

console.log(chernii);
console.log(another);


const batya = new Employee('Oleg', 'Kaban', 25);

console.log(batya.name , batya.nickname);
batya.calculate(1);



for (const id in jobs) {
	if (jobs.hasOwnProperty(id)) {
		for(const k in jobs[id]) {
			console.log(jobs[id][k]);
		}
	}
}
