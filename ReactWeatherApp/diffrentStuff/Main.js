import React, { Component } from 'react';
// import './App.css';

import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import styled from 'styled-components';

const Repair = styled.div`
	color: #ffffff;
	font-size: 40px;
	font-weight: 700;
	line-height: 30px;

	span {
		margin-top: 10px;
		display: block;
		font-size: 24px;
	}
`

const Description = styled.div`
	color: #ffffff;
	margin-top: 54px;
	font-size: 18px;
	font-weight: 300;
	line-height: 28px;
`
const CallButton = styled.button`
	width: 247px;
	height: 67px;
	margin-top: 30px;
	background-color: #ffa14b;
	border-radius: 30px;
	color: #ffffff;
	font-size: 18px;
	font-weight: bold;
	line-height: 24px;
`


class Main extends React.Component {
	render() {
		return (
			<Row>
				<Col lg={5}>
					<Repair>
						Качественный ремонт
						<span>iphone за 35 минут и гарантия 1 год</span>
					</Repair>
					<Description>
						Оставьте заявку на бесплатную диагностику без очереди,
						и получите защитное стекло, стоимостью 1000 рублей,
						с установкой в подарок!
					</Description>
					<CallButton>
						Отправить заявку!
					</CallButton>
				</Col>
				<Col lg={6} lgOffset={1}>

				</Col>
			</Row>
		)
	}
}

export default Main
