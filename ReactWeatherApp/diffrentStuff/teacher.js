import { Person } from './person';

export default class Employee extends Person {
	constructor(name,nickname, age) {
		super(name, nickname)
		this.age = age;
	}
	sing() {
		console.log(`Its a beautiful day to sing, dont u think ${this.name}`);
	}
	calculate(n) {
		for(let i = 1; i <= 5; i++) {
			console.log(`${this.name} is adding ${i} to ${n}, and once again! And we get ${n+i}`);
		}
	}
}