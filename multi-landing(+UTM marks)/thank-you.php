<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="/favicon.ico">
	<style>
		body {
            background: linear-gradient(#ff9a9e, #fad0c4);
            height: 100vh;
          }
					main {
						padding-top: 5%;
						padding-bottom: 5%;
					}
          h2 {
            text-align: center;
          }
					form {
						background-color: #fad0c4;
            border-radius: 10px;
						padding: 20px;
						margin-top: 20px;
					}
          .img_block {
            margin-top: 70px;
          }
					img {
            margin-top: 150px;
            border-radius: 20px;
						max-width: 400px;
						width: 100%;
						display: block;
						margin: auto;
					}
          h2 {
            color: #fff;
            text-transform: uppercase;
          }
          .btn-order {
            display: inline-block;
            padding: 10px 25px;
			font-size: 18px;
			font-weight: bold;
            border: none;
            border-radius: 2px;
            color: #454545;
            text-align: center;
            position: relative;
            left: 50%;
            transform: translateX(-50%);
          }
		  .h2 {
			  text-align: center;
		  }
		  .right {
			  padding: 30px;
			  text-align: center;
			  
		  }
		  .thx { 
			  font-size: 18px;
		  }

				</style>
	<title>Flowers</title>

	<!-- Bootstrap core CSS -->
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

	<?php
    $text = 'Flowers';
    $utm_term = $_GET['utm_term'];
    if (!empty($utm_term)) {
      if ($utm_term == 'rose') {
        $text = 'roses';
      }
      if ($utm_term == 'hrizantema') {
        $text = 'chrysanthemums';
      }
      if ($utm_term == 'herber') {
        $text = 'gerberas';
      }
    }
    ?>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<main>
		<div class="container">
			<div class="row align-items-center">
				<div class="col-sm-6">
					<div class="img_block">
						<img src="img/<?php if (!empty($utm_term)) {
              echo $utm_term;
            } else { echo 'flowers'; }?>.png"
						 alt="">
					</div>
				</div>
				<!-- /.col-sm-6 -->
				<div class="col-sm-6">
					<div class="right">
					<h2 class="h2">Thank your for your order</h2>
					<p class="thx">
						Your <?php echo $text ?> are coming
					</p>
					</div>
				</div>
				<!-- /.col-sm-6 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container -->
	</main>


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
	<!-- jQuery Mask Input -->
	<script type="text/javascript" src="libs/jquery.maskedinput.min.js"></script>
	<script>
		$(document).ready(function () {
			$("#phone").mask("+351 999 99 99")
		});
	</script>
</body>

</html>