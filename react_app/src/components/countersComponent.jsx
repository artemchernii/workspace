import React, { Component } from "react";
import Counter from "./counterComponent";

class Counters extends Component {
  componentDidUpdate(prevProps, prevState) {
    console.log("prevProps", prevProps);
    console.log("prevState", prevState);
    // if (prevProps.counter.value !== this.props.counter.value) {
    //   // Ajax call and get new data from server
    // }
  }
  componentWillUnmount() {
    console.log("Counter - Unmout");
  }
  render() {
    const { onReset, onDelete, onIncrement } = this.props;
    console.log("Counters - rendered");
    return (
      <div>
        <button onClick={onReset} className="btn btn-primary btn-sm m-2">
          Reset
        </button>
        {this.props.counters.map(counter => (
          <Counter
            key={counter.id}
            onDelete={onDelete}
            onIncrement={onIncrement}
            counter={counter}
          />
        ))}
      </div>
    );
  }
}

export default Counters;
