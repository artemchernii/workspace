let clock = document.getElementById("clocks"),
	color = document.getElementById("color"),
	text = document.querySelector(".text");


function hexClocks() {
	let time = new Date(),
		h = time.getHours(),
		m = time.getMinutes(),
		s = time.getSeconds();

	if (s < 10) {
		s = "0" + s;
	}
	if (m < 10) {
		m = "0" + m;
	}
	if (h < 10) {
		h = "0" + h;
	}
	clock.textContent = h + ":" + m + ":" + s;
	color.textContent = `background = #${h}${m}${s}`;
	text.innerHTML = `Right is ${h} hours`;
	document.body.style.background = "#" + h + m + s;
	console.log(h);
	console.log(m);
	console.log(s);
}


hexClocks();
setInterval(hexClocks, 1000);