document.addEventListener('DOMContentLoaded', function () {
	let showIp = document.querySelector("#show_ip"),
		myId = document.querySelector("#myip");

	window.onload = function () {
		showIp.addEventListener("click", () => {
			ajaxGet();
		});
	}


	ajaxGet = () => {
		let request = new XMLHttpRequest();
		request.onreadystatechange = function () {
			if (request.readyState == 4 && request.status === 200) {
				myId.innerHTML = request.responseText;
			}
		}
		request.open('GET', 'server.php');
		request.send();
	}


}, false);