import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import NewRoute from "./components/NewRoute";
import Home from "./components/Home";
import About from "./components/About";
import Contacts from "./components/Contacts";
import Error from "./components/Error";
import Navigation from "./components/Navigation";

import "./App.css";

class App extends Component {
  state = {
    real: ""
  };
  runFunc = () => {
    this.setState({
      open: true,
      real: "Shit"
    });
  };
  render() {
    return (
      <React.Fragment>
        <BrowserRouter>
          <div>
            <Navigation />
            <Switch>
              <Route path="/" component={Home} exact />
              <Route path="/about" component={About} />
              <Route path="/contacts" component={Contacts} />
              <Route path="/new" component={NewRoute} />
              <Route component={Error} />
            </Switch>
          </div>
        </BrowserRouter>
        <button onClick={this.runFunc.bind(this)}>Click</button>
      </React.Fragment>
    );
  }
}

export default App;
