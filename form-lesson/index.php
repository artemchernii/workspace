<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>SMTP form</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <style>
    	form {
    		background-color: #f9f9f9;
    		padding: 20px;
    		margin: 20px;
    	}
    </style>
  </head>

  <body>
    <div class="container">

      <div class="row">
      	<div class="col-sm-4">
      		<form action="mail.php" method="POST" enctype="multipart/form-data">
      			<legend>Form Title</legend>
						<!-- hidden fields -->

						<input type="hidden" name="utm_source" value="<?php $utm_source = $_GET['utm_source']; echo $utm_source; ?>">
						<input type="hidden" name="utm_medium" value="<?php $utm_medium = $_GET['utm_medium']; echo $utm_medium; ?>">
						<input type="hidden" name="utm_campaign" value="<?php $utm_campaign = $_GET['$utm_campaign']; echo $utm_campaign; ?>">
						<input type="hidden" name="utm_content" value="<?php $utm_content = $_GET['$utm_content']; echo $utm_content; ?>">
						<input type="hidden" name="utm_term" value="<?php $utm_term = $_GET['$utm_term']; echo $utm_term; ?>">
						<!-- hidden fields -->

      			<div class="form-group">
      				<label for="">Name</label>
      				<input type="text" class="form-control" id="" name="user_name" placeholder="John Black" required>
      			</div>

      			<div class="form-group">
      				<label for="">Phone</label>
      				<input type="text" class="form-control" id="" name="user_phone" placeholder="+351 999 73 73" required>
      			</div>

      			<div class="form-group">
      				<label for="">Email</label>
      				<input type="text" class="form-control" id="" name="user_email" placeholder="John@gmail.com" required>
						</div>

						<div class="form-group">
								<label for="">Photo</label>
								<input type="file" class="form-control" id="" name="user_photo" placeholder="Your photo" required>
							</div>

      			<button type="submit" class="btn btn-primary">Send</button>
      		</form>
      	</div><!-- .col-sm-4 -->
      </div> <!-- .row -->

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
