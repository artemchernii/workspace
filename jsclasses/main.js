const form = document.getElementById("my-form");
const h1 = document.getElementsByTagName("h1");
const allitems = document.querySelector(".items");
const msg = document.querySelector(".msg");
const name = document.getElementById("name");
const email = document.getElementById("email");
const btn = document.querySelector(".btn");
let onSubmit = (e) => {
	e.preventDefault();
	let nameValue = name.value;
	let emailValue = email.value;
	if (emailValue === "" || nameValue === "") {
		msg.innerHTML = `<h3 style="color: red; background: #000000">Please enter some information</h3>`;
		setTimeout(() => {
			msg.innerHTML = "";
		}, 1000);
	} else {
		let div = document.createElement("li");
		allitems.appendChild(div);
		div.innerHTML = `
			<h3>${nameValue}</h3>
			<p>${emailValue}</p>
		`;
		setTimeout(() => {
			name.value = "";
			email.value = "";
		}, 1500);
	}

}


form.addEventListener("submit", onSubmit);


















// let h11 = h1[0].cloneNode(true);
// btn.addEventListener("click", (e) => {
// 	e.preventDefault();
// 	h1[0].innerText = "New title";

// 	allitems.appendChild(h11);
// 	console.log(h11);
// });
// allitems.firstElementChild.textContent = "Nike";
// allitems.children[1].textContent = "Petya";
// allitems.childNodes.forEach((item) => {
// 	item.innerHTML = `
// 	<h4>Artem is not stupid</h4>
// 	`;
// });
// allitems.remove();

// let num = 0;
// window.addEventListener("resize", function () {
// 	console.log(this.innerWidth);
// 	num += this.innerWidth;
// 	document.body.innerHTML = num;
// })
































// // Функция конструктор
// var Person = function (name) {
// 	// Приватная функция
// 	var log = function (message) {
// 		console.log(message);
// 	};

// 	// Публичное свойство
// 	this.name = name;

// 	// Публичный метод
// 	this.logger = function (message) {
// 		log('Public ' + message);
// 	};
// };


// // class Countdown {
// // 	constructor(count, action) {
// // 		Object.assign(this, {
// // 			dec() {
// // 				if (count < 1) return
// // 				count--;
// // 				if (count === 0) {
// // 					action();
// // 				}
// // 			}
// // 		})
// // 	}
// // }

// // let c = new Countdown(4, () => console.log('Done'));

// // c.dec();
// // c.dec();




// var Name = function (name) {
// 	this.name = name;
// 	console.log(this.name);

// 	var greet = function () {
// 		console.log(`Hello ${this.name}`);
// 	}
// }





// var sayName = function (name) {
// 	this.name = "Your name " + name;
// 	console.log(`${this.name}`);
// }

// sayName("Artem");

// // Дед попугай с двумя лапами
// var ParrotGrandfather = function () {};
// ParrotGrandfather.prototype = {
// 	species: 'Parrot',
// 	paws: 2
// };

// // Отец попугай унаследовал всё у деда
// var ParrotFather = function () {};
// ParrotFather.prototype = Object.create(ParrotGrandfather.prototype);

// var ParrotSon = function () {};
// ParrotSon.prototype = Object.create(ParrotFather.prototype);



// var grandfather = new ParrotGrandfather(),
// 	father = new ParrotFather(),
// 	son = new ParrotSon();


// ParrotGrandfather.prototype.paws++;
// ParrotFather.prototype.species = "Eagle";
// ParrotSon.prototype.paws--;
// ParrotGrandfather.prototype.species = "Seagull";

// console.log(grandfather.species, father.species, son.species);
// console.log(grandfather.paws, father.paws, son.paws);

// // Переназначение метода toString для всех объектов,
// // созданных с помощью данного конструктора
// Person.prototype.toString = function () {
// 	return 'Person ' + this.name;
// };

// var john = new Person('John');

// john.toString();









// // Два массива, второй абсолютно обычный,
// // для первого переназначен метод toString
// var arr1 = [4, 2];
// var arr2 = [5, 3];
// arr1.toString = function () {
// 	return 'Array ' + this.reduce(function (result, item) {
// 		return result + '' + item;
// 	});
// };

// // В итоге
// console.log(john.toString()); // Person John
// console.log(arr1.toString()); // Array 42
// console.log(arr2.toString()); // 5,3





// class Human {
// 	constructor(name, age) {
// 		this.name = name;
// 		this.age = age;
// 	}
// 	sayName() {
// 		console.log(`Hello friend ${this.name}`);
// 	}
// }

// class GreatHuman extends Human {
// 	constructor(name, age, phrase) {
// 		super(name, age);
// 		this.phrase = phrase;
// 	}
// 	sayPhrase() {
// 		console.log(`${this.name} says: "${this.phrase}"`);
// 	}
// 	sayAge(n) {
// 		console.log(`I'm ${this.name} and i'm ${n} years old.`);
// 	}
// }

// let kolya = new GreatHuman("Kolya", 23, "I love Lilya");

// kolya.sayAge(2);




// class Speaker extends GreatHuman {
// 	constructor(name, age, phrase, nationality) {
// 		super(name, age, phrase)
// 		this.nationality = nationality;
// 	}
// 	newSay() {
// 		console.log(`${this.name} very confident! And i'm ${this.nationality} ${this.sayAge(25)}`);
// 	}
// }



// class CoolGuy {
// 	constructor(name) {
// 		this.name = name;
// 	}
// 	speak(phrase) {
// 		return `${this.name} says: "${phrase}"`;
// 	}
// }

// class Cooler extends CoolGuy {
// 	speak(phrase) {
// 		console.log(`${super.speak(phrase)} very confident!`);
// 	}
// 	sayCoolShit(shit) {
// 		console.log(`${shit} -> thats what i'm talking about`);
// 	}
// }




// class TypicalDog {
// 	constructor(name, legs, color) {
// 		this.name = name;
// 		this.legs = legs;
// 		this.color = color;
// 	}
// 	greet() {
// 		console.log(`${this.name} with ${this.legs} legs has pretty ${this.color} color`);
// 	}
// }

// class SmallDog extends TypicalDog {
// 	constructor(name, legs, color, style) {
// 		super(name, legs, color);
// 		this.style = style;
// 	}
// 	play() {
// 		console.log(`${this.name} with ${this.legs} legs likes to play ${this.style}`);
// 	}
// }



// let topa = new TypicalDog('Topa', 4, 'Brown');
// let rex = new SmallDog('Rex', 2, 'Black', 'Run');

// rex.play();
// topa.greet();





// const John = new GreatHuman('John', 'I am cool guy!');
// var Artem = new GreatHuman('Artem', 'I am trying');



// // Artem.sayPhrase();
// // Artem.name = "Petya";
// // Artem.sayPhrase();
// // John.sayPhrase();



// // ES 5

// // var Valera = function(name) {
// //   this.name = name;
// // }
// //
// // Valera.prototype.phrase = function (phrase) {
// //   return `${this.name} says: "${phrase}"`;
// // }
// //
// // var Speakz = function (name) {
// //   Valera.call(this, name);
// // }
// // Speakz.prototype = Object.create(Valera.prototype);
// // Speakz.prototype.phrase = function phrase() {
// //   var originPhrase = Valera.prototype.phrase;
// //   console.log(originPhrase.call(this, phrase) + ' very confident');
// // }
// // var Oleg = new Speakz("Olejka");
// // Oleg.phrase('I am very cool');

// var roflan = function (name, age, arr) {
// 	this.name = name;
// 	this.age = age;
// 	this.arr = arr;
// }

// roflan.prototype.sayName = function SayName() {
// 	return 'My Name is ' + this.name;
// }
// // roflan.prototype.phrases = function phrases(arr) {
// //   arr.forEach(function(ele) {
// //     return  ele + 'roflan';
// //   })
// // }
// roflan.prototype.count = function (arr) {
// 	arr.forEach(function (ele) {
// 		console.log(ele);
// 	})
// }
// roflan.prototype.rofl = function (arr) {
// 	arr.forEach(function (ele) {
// 		console.log(ele + " rofl");
// 	})
// }

// class Rofl {
// 	constructor(theme) {
// 		this.theme = theme;
// 	}
// 	act(game) {
// 		console.log(`${game} is so fucking ${this.theme}`);
// 	}
// }

// let kek = new Rofl('smile');

// kek.act("Dota");

// let lol = document.querySelector('.LOL'),
// 	text = document.querySelector('.text'),
// 	newel = document.createElement('DIV');

// lol.addEventListener("click", () => {
// 	text.textContent = "LEL";
// 	newel.classList.add("roflan");
// 	newel.textContent = "ROFLAN";
// 	text.appendChild(newel);
// });



// // function multiplyElem(str) {
// //   return str + "EBALO";
// // }


// var Olejka = new roflan('Olejka', 16, ['Roflan', 'Pominki', 'Kek']);

// console.log(Olejka.sayName());
// console.log(Olejka.rofl(Olejka.arr));


// // function rofl(arr) {
// //   arr.forEach(function (ele) {
// //     ele + "Rofl";
// //   })
// // }

// class Papich {
// 	constructor(name, age, location) {
// 		this.name = name;
// 		this.age = age;
// 		this.location = location;
// 	}
// 	hello() {
// 		console.log(`Privet pacani , eto ya ${this.name}`);
// 	}
// }

// let Olez = new Papich('Papanya', 25, 'Vinnitsa');



// /*  Learn JS   */


// function MyArray() {}
// MyArray.prototype = [];

// let arr = new MyArray();
// arr.push(1, 2, 3);
// console.log(arr.length);

// // for(let i = 0; i < 10; i++) {
// // 	setTimeout(function() {
// // 		console.log(i);
// // 	}, 100);
// // }




// let obj = {
// 	"a": 1,
// 	0: 2
// }
// sum = [] + false - null + true;

// f.call(null);

// function f() {
// 	alert(this);
// }

// console.log(sum);
// console.log(obj['a']);


// let arr = [2, 2, 3, 3, 5];

// let arr2 = [2, 12, ...arr]


// const sum = (arr) => {
// 	res = arr.reduce((x, y) => x + y);
// 	return res;

// }

// const add = (arr) => {
// 	let total = 0;
// 	arr.forEach(element => {
// 		total += element;
// 	});
// 	return total;

// }


// const rechange = (arr) => {
// 	let newArr = arr.filter((n) => n > 3);
// 	return newArr;
// }


// console.log(rechange(arr));





// let x = 0,
// 	xx = "abc",
// 	xxx = false,
// 	xxxx = undefined,
// 	xxxxx = {},
// 	xxxxxx = Symbol,
// 	xxxxxxx = [];
// let arr = [];
// console.log(
// 	`
// 		This is object - ${typeof(xxxxx)} and this is an array - ${typeof(xxxxxxx)}.
// 	`
// );

// let consoleLog = (el) => {
// 	console.log(el);
// }

// consoleLog(typeof (x));
// consoleLog(typeof (xx));
// consoleLog(typeof (xxx));
// consoleLog(typeof (xxxx));
// consoleLog(typeof (xxxxx));
// consoleLog(typeof (xxxxxx));
// consoleLog(typeof (xxxxxxx));

// let text = document.querySelector(".text"),
// 	btn = document.querySelector(".LOL"),
// 	input = document.querySelector("#input");

// btn.addEventListener("click", () => {
// 	text.innerHTML = input.value;
// 	let newtext = new String;
// 	newtext = prompt();
// 	document.body.style.backgroundColor = newtext;
// 	console.log(input.value)
// 	console.log(newtext);
// })
// const name = "Artem";
// const ready = ` We are ready to work, let's go ${name}`;
// const lorem = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
// const apple = ["Iphone", "Watches", "Mac"];
// const person = {
// 	firstName: "Artem",
// 	lastname: "Chernii",
// 	age: 25,
// 	hobbies: ["nothing", "tv"],
// 	address: {
// 		steet: "Kosmonavtov",
// 		city: "Vinnitsa",
// 		country: "Ukraine"
// 	},
// 	sayHi: () => {
// 		console.log(`Hey, my name is ${person.firstName}`);
// 	}
// }

// const todos = [{
// 		id: 1,
// 		text: 'Take out trash',
// 		isCompleted: true
// 	},
// 	{
// 		id: 2,
// 		text: 'Wash dishes',
// 		isCompleted: false
// 	},
// 	{
// 		id: 3,
// 		text: 'Take out dog',
// 		isCompleted: false
// 	}
// ]
// for (let todo of todos) {
// 	console.log(todo.text);
// }
// const todoJSON = JSON.stringify(todos);

// todos.forEach(todo => {
// 	console.log(todo)
// });
// const todoComplited = todos.filter(todo => {
// 	return todo.isCompleted === true;
// }).map(todo => {
// 	return todo.text;
// });
// let x = 10;

// let color = x > 10 ? 'yellow' : 'tomato';
// const red = "tomato";

// switch (red) {
// 	case "yellow":
// 		console.log("color is red");
// 		break;
// 	case "tomato":
// 		console.log("color is tomato");
// 		break;
// 	default:
// 		console.log("Not your color");
// 		break;
// }

// function addNums(num1, num2) {
// 	return num1 + num2;
// }
// let adds = (num1 = 0, num2 = 0) => num1 + num2;

// function Person(firstName, lastName, dob) {
// 	this.firstName = firstName;
// 	this.lastName = lastName;
// 	this.dob = new Date(dob);
// }

// Person.prototype.getFullName = function () {
// 	return `${this.firstName} ${this.lastName}`;
// }

// Person.prototype.getBirthYear = function () {
// 	return this.dob.getFullYear();
// }

// class Kent {
// 	constructor(firstName, lastName, dob) {
// 		this.firstName = firstName;
// 		this.lastName = lastName;
// 		this.dob = new Date(dob);
// 	}
// 	getFullName() {
// 		return `${this.firstName} ${this.lastName}`;
// 	}
// 	getBirthYear() {
// 		return this.dob.getFullYear();
// 	}
// }



// const kent = new Kent("Oleg", "Kaban", "05-20-1994");

// const person1 = new Person("Artem", "Chernii", "12-14-1993");
// const person2 = new Person("Tanya", "Mavliudova", "02-01-1998")
// console.log(person1.getBirthYear());
// console.log(person2.getFullName());
// console.log(person1);
// console.log(kent);
// window.addEventListener("load", function () {});