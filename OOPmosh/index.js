// function Timer() {
// 	let startTime, endTime, running, duration = 0;
// 	this.start = function () {
// 		if (running)
// 			throw new Error('Timer is running');
// 		running = true;

// 		startTime = new Date();
// 		console.log(startTime);
// 	};
// 	this.stop = function () {
// 		if (!running)
// 			throw new Error('Timer is now started')

// 		running = false;

// 		endTime = new Date();

// 		const seconds = (endTime.getTime() - startTime.getTime()) / 1000;
// 		duration += seconds;
// 	};
// 	this.reset = function () {
// 		startTime = null;
// 		endTime = null;
// 		running = false;
// 		duration = 0;
// 	};
// 	Object.defineProperty(this, 'duration', {
// 		get: function () {
// 			return duration;
// 		}
// 	});
// }

// class Timmer {
// 	constructor() {
// 		this.startTime = 0;
// 		this.stopTime = 0;
// 		this.running = 0;
// 		this.duration = 0;
// 	}
// 	start() {
// 		if (this.running)
// 			throw new Error('Timer is running');
// 		this.running = true;

// 		let startTime = new Date();
// 		console.log(startTime);
// 	}
// 	stop() {
// 		if (!this.running)
// 			throw new Error('Timer is now started')

// 		this.running = false;

// 		let endTime = new Date();

// 		const seconds = (endTime.getTime() - startTime.getTime()) / 1000;
// 		duration += seconds;
// 	}
// 	reset() {
// 		startTime = null;
// 		endTime = null;
// 		running = false;
// 		duration = 0;
// 	}
// 	duration() {
// 		return this.duration();
// 	}
// }

// let tiime = new Timmer();

// let time = new Timer();


//Factory Function
function createFunction(radius, location) {
	return {
		radius,
		location,
		draw() {
			console.log('Draw');
		}
	};
}

const circle = createFunction(2, [1, 1]);

console.log(circle.draw());

//Constructor Function

function Circle(radius) {
	this.radius = radius;
	console.log(this.radius);
	this.draw = function () {
		console.log('draw');
	}
}

let circleOne = new Circle(3);
circleOne.draw();
// //Setter and Getter

// const person = {
// 	fistName : "Artem",
// 	lastName : "Chernii",
// 	get fullName() {
// 		return `${person.firstName} ${person.lastName}`
// 	},
// 	set fullName(value) {
// 		const parts = value.split(' ');
// 		this.firstName = parts[0];
// 		this.lastName = parts[1];
// 	}
// }
// person.fullName = "Oleg Kaban";

// console.log(person.fullName);

// //Cloning an Object

// const buble = {
// 	radius: 1,
// 	draw() {
// 		console.log('Draw');
// 	}
// }


// const another = {};


// for (const key in buble)
// 	another[key] = buble[key];



// // const another = Object.assign({}, buble);


// // const another = { ...buble };

// another.draw();

// this
const video = {
	title: 'a',
	tags: ['a', 'b', 2],
	play() {
		console.log(this);
	},
	showTags() {
		this.tags.forEach(function (tag) {
			console.log(`${tag} ${this.title}`);
		}, this);
	}
}
video.showTags();

// function Video(title) {
// 	this.title = title;
// 	console.log(this);
// }

// let game = new Video('LOL');

// //filter

const arr = [1, 2, 3, 25];


// const res = arr.filter(n => n < 10).map(n => n * 2);
const res = arr.reduce((acc, cur) => acc + cur);


console.log(res);


// const items = arr
// 	.filter(n => n >= 2)
// 	.map(n => ({value: n}))
// 	.filter(p => p.value > 2)
// 	.map(p => p.value);


// // const html = `<ul> ${items.join('')} </ul>`;

// console.log(items);


// //reduce


// const numbers = [-1, 1 , 2 , 3 , 4 , 25];

// let sum = 0;

// for (const n of numbers) {
// 	sum += n;
// }

// console.log(sum);


// const result = numbers.reduce(
// 	(accumulator, currentValue) => accumulator + currentValue, 0);

// console.log(result);
