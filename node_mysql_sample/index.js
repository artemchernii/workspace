const express = require("express");
const path = require("path");

const app = express();

app.use(express.json());
app.use(
  express.urlencoded({
    extended: false
  })
);

app.post("/login", (req, res) => {
  if (!req.header("x-auth-token")) {
    return res.status(400).send("No token");
  }

  if (req.header("x-auth-token") !== "123456") {
    return res.status(400).send("Un authorized");
  }

  res.send("Log in");
});

app.listen(5000, () => console.log(`Server started at 5000`));
