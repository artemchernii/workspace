document.getElementById('myForm').addEventListener('submit', saveBookmark);

function saveBookmark (e) {
	console.log('It works!');
	let siteName = document.getElementById('siteName').value;
	let siteUrl = document.getElementById('siteURL').value;

	if (!validateForm(siteName, siteUrl)) {
		return false;
	}

	var bookmark = {
		name : siteName,
		url : siteUrl
	}

	if (localStorage.getItem('bookmarks') === null) {
		var bookmarks = [];
		bookmarks.push(bookmark);
		localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
	} else { 
		var bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
		bookmarks.push(bookmark);
		localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
	}
	setTimeout(() => {
		document.getElementById('myForm').reset();
	}, 300);
	fetchBookmarks();
	e.preventDefault();
}

function deleteBookmark(url) { 
	var bookmarks = JSON.parse(localStorage.getItem('bookmarks'));

	for (let i = 0; i < bookmarks.length; i++) {
		if (bookmarks[i].url == url) {
			bookmarks.splice(i,1);
		}
	}
	localStorage.setItem('bookmarks',JSON.stringify(bookmarks));
	fetchBookmarks();
 }

function fetchBookmarks () { 
	var bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
	var bookmarksResults = document.getElementById('bookmarks-results');

	bookmarksResults.innerHTML = "";

	for (let i = 0; i < bookmarks.length; i++) {
		var name = bookmarks[i].name;
		var url = bookmarks[i].url;
		
		bookmarksResults.innerHTML += '<div class="card mb-2 ">'+
			'<h3>'
				+ name +
				' <a class="btn btn-outline-secondary" target="_blank" href="'+url+'">Visit</a>' +
				' <a onclick="deleteBookmark(\''+url+'\')" class="btn btn-outline-danger" href="#'+url+'">Delete</a>' +
			'</h3>' +
		'</div>';
	}

}

var clearBtn = document.getElementById('clear-list');

clearBtn.addEventListener('click', () => {
	localStorage.clear('bookmarks');
	var bookmarks = [];
	console.log(bookmarks);
});

function validateForm(siteName, siteUrl) {
	if (!siteName || !siteUrl) {
		alert("Please, fill in the form");
		document.getElementById('myForm').reset();
		return false;
	}
	var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
	var regex = RegExp(expression);

	if (!siteUrl.match(regex)) {
		alert("Please, use a valid URL");
		document.getElementById('myForm').reset();
		return false;
	}
	return true;
}

// Local Storage TEST 
	// localStorage.setItem('test', 'Hello World');
	// console.log(localStorage.getItem('test'));
	// localStorage.removeItem('test');
	// console.log(localStorage.getItem('test'));